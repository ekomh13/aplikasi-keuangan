package id.ac.tazkia.aplikasikeuangan.controller.masterdata;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PegawaiController {

    @GetMapping("/masterdata/pegawai")
    public String dashboardUtama(Model model) {


        return "/masterdata/pegawai/list";

    }

}
