package id.ac.tazkia.aplikasikeuangan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class DashboardController {

    @GetMapping("/dashboard")
    public String dashboardUtama(Model model) {

        return "dashboard";

    }

    @GetMapping("/")
    public String formAwal(){
        return "redirect:/dashboard";
    }

}
